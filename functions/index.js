const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
 service: 'gmail',
 auth: {
        user: 'Kgatlejoseph@gmail.com',
        pass: '0787253289'
    }
});



exports.sendContactMessage = functions.database.ref('/bookings/{pushKey}').onWrite(event => {
  const snapshot = event.data;
// Only send email for new messages.
  if (snapshot.previous.val() || !snapshot.val().name) {
     return;
  }
  
  const val = snapshot.val();
  
  const mailOptions = {
	from: val.mail,
    to: 'Kgatlejoseph@gmail.com',
    subject: 'Booking',
    html:  'You have a booking request. <br> <br>'+ '<b>Booking from<b> <br>' +'Email: '+val.mail+'<br>Name:'+val.name+'<br>Phone: '+val.cNumber
  };
  
  const mailOptionsForClient = {
	from: 'Kgatlejoseph@gmail.com',
    to: val.mail,
    subject: 'Booking received',
    html:  'Dear '+val.name+' <br> <br>Your booking has been received. I will get back to you. Thanks.<br><br> <br> Regards,<br>Josie Keen'
  };
  
 
  
  transporter.sendMail(mailOptions, function (err, info) {
   if(err)
     console.log(err)
   else
     console.log(info);
});


 transporter.sendMail(mailOptionsForClient, function (err, info) {
   if(err)
     console.log(err)
   else
     console.log(info);
});



 
});



exports.sendMessage = functions.database.ref('/messages/{pushKey}').onWrite(event => {
  const snapshot = event.data;
// Only send email for new messages.
  if (snapshot.previous.val() || !snapshot.val().name) {
     return;
  }
  
  const val = snapshot.val();
  
  const mailOptions = {
	from: val.mail,
    to: 'Kgatlejoseph@gmail.com',
    subject: 'Message',
    html:  val.message +'<br> <br> <br> <b>Sender Details</b> <br><br> Email: '+val.mail+'<br>Name:'+val.name
  };
  
  const mailOptionsForClient = {
	from: 'Kgatlejoseph@gmail.com',
    to: val.mail,
    subject: 'Message received',
    html:  'Dear '+val.name+' <br> <br>Your message has been received. Thanks.<br><br> <br> Regards,<br>Josie Keen'
  };
  
 
  
  transporter.sendMail(mailOptions, function (err, info) {
   if(err)
     console.log(err)
   else
     console.log(info);
});


 transporter.sendMail(mailOptionsForClient, function (err, info) {
   if(err)
     console.log(err)
   else
     console.log(info);
});



 
});
