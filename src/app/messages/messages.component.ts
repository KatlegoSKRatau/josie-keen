import { Component, OnInit, NgZone } from '@angular/core';
 import * as firebase from 'firebase/app';
 import { Router } from '@angular/router';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

 list:any;
	persons:any;
	
	isCorrect:boolean = false;
	countMessages: number;
	bookingsLoaded: boolean ;
	
	domRefresh: boolean = false;
	
    constructor(private router : Router, private zone: NgZone) { 

    }
  
  onDelete(message)
  {
		
	    firebase.database().ref('messages/' + message.key).remove();
		this.ngOnInit();
  }
  
    onLogout()
	{

		localStorage.removeItem("admin");

		this.router.navigate(['/admin']);
		
	
	}

  ngOnInit() {
  
         
  
	      if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
			
			if(localStorage.getItem("admin")=="yes"){
				    
					 this.countMessages = 0;
					 this.bookingsLoaded = false;
					 this.domRefresh = false;
					
					
					this.list = firebase.database().ref('/messages');
					 this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
					
					
					dataSnapshot.forEach((childSnapshot) => {
					
					 let person  = childSnapshot.val();
					 person.key = childSnapshot.key;
					 this.countMessages +=1;
					 this.persons.push(person);
					 
								
							 
							 
							 

					});
					
					    if(this.persons.length == 0)
						{
						
							this.bookingsLoaded = true;
						
						}else{
						
							
							if(this.countMessages == this.persons.length )
							 {
										
										this.bookingsLoaded = true;
										this.domRefresh = true;
										
										this.zone.run(() => {});
										
							 }
						
						
						}
					
					     
			
           
          });
			
			
			
			}
		
		
		
         
  }

}
