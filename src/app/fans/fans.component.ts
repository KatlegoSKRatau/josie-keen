import { Component, OnInit, NgZone } from '@angular/core';
import {Http} from '@angular/http';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-fans',
  templateUrl: './fans.component.html',
  styleUrls: ['./fans.component.css']
})
export class FansComponent implements OnInit {

   hasSent: boolean = false;
   isWait: boolean  = false;
   errorOccured: boolean = false;
	  
	list:any;
	persons:any;
	
	isCorrect:boolean = false;
	
	countEvents: number;
	eventsLoaded: boolean;
	
	domRefresh: boolean = false;
	
	imageSrc: string;
	
  constructor( private http: Http, private router: Router, private zone : NgZone) { }

  ngOnInit() {
			if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
			
			
			
			if(localStorage.getItem("admin")=="yes")
			{
			    
			    this.eventsLoaded = false;
				this.countEvents = 0;
				this.domRefresh = false;
				
				this.list = firebase.database().ref('/fans');
				 this.list.on('value', (dataSnapshot)=> {
				this.persons = [];
	  
				dataSnapshot.forEach((childSnapshot) => {
				  let person = childSnapshot.val();
				  person.key = childSnapshot.key
				  
				  this.countEvents +=1;
				   this.persons.push(person);

				});
				
				
				if(this.persons.length == 0)
				{
				
						 this.eventsLoaded = true;
				
				}else{
				
				if(this.countEvents == this.persons.length )
				  {
				     this.eventsLoaded = true;
					 this.domRefresh  = true;
					 
					 this.zone.run(() => {});
					 
				  }
				
				}
				
				 
			
		
           
          });
			
			
		}
			
			
			
  }
  
   onSubmit(form)
  {
	

	
	this.isWait = true;
	
	let fan = {
			
			fanName : form.value.title,
			pic : this.imageSrc
	
	}
	
		 this.http.post('https://jkpro-jk.firebaseio.com/fans.json',fan).subscribe((response)=>{
					
					this.isWait = false;
					this.hasSent = true;
					setTimeout(()=>{   
					  this.hasSent = false;
					},3000);
					this.ngOnInit();
					form.reset();
					
					
		 }, (error)=>{
				this.isWait = false;
				this.errorOccured = true;
				
				setTimeout(()=>{   
					  this.errorOccured = false;
			    },3000);
		 })
		
  }
  
  
   handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    /*if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }*/
	
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
   
  }
  
    onLogout()
	{

		localStorage.removeItem("admin");

		this.router.navigate(['/admin']);
		
	
	}
	
	
 onDelete(event)
 {
	
	  firebase.database().ref('fans/' + event.key).remove();
	  this.ngOnInit();
		
 
 }
  
  
  

}
