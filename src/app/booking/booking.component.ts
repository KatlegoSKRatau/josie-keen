import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {Http} from '@angular/http';
@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {


	  hasSent: boolean = false;
	  isWait: boolean  = false;
	  errorOccured: boolean = false;

	  
  constructor(private router: Router, private http: Http) { 
	this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
			
                return;
            }
            window.scrollTo(0, 0);
			
        });
  
  }
  
  onSubmit(form)
  {
	

	this.isWait = true;
	
		 this.http.post('https://jkpro-jk.firebaseio.com/bookings.json',form.value).subscribe((response)=>{
					
					this.isWait = false;
					this.hasSent = true;
					setTimeout(()=>{   
					  this.hasSent = false;
					},4000);
					
					form.reset();
					
		 }, (error)=>{
				this.isWait = false;
				this.errorOccured = true;
				
				setTimeout(()=>{   
					  this.errorOccured = false;
			    },4000);
		 })
		
  }

  ngOnInit() {
  }

}
