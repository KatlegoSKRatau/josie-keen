import { Component, OnInit, NgZone } from '@angular/core';
import { FirebaseApp } from 'angularfire2';

import { AngularFireStorage } from 'angularfire2/storage';
import {Http} from '@angular/http';
import * as firebase from 'firebase/app';

import { Router } from '@angular/router';

@Component({
  selector: 'app-mixtape',
  templateUrl: './mixtape.component.html',
  styleUrls: ['./mixtape.component.css']
})
export class MixtapeComponent implements OnInit {

  selectedFiles:any;
  imgsrc:any;
  
  	  hasSent: boolean = false;
	  isWait: boolean  = false;
	  errorOccured: boolean = false;
	  
	  
	   list:any;
	   persons:any;
	   isCorrect: boolean = false;
	   
	   countMixTapes: number;
	   mixTapeLoaded: boolean;
	   
	   domRefresh: boolean = false;
  
  constructor(private firebaseApp: FirebaseApp,private storage: AngularFireStorage, private http: Http, private router: Router, private zone : NgZone) { }

  onUpload(form)
  {
		
		this.isWait = true;
	
		 this.http.post('https://jkpro-jk.firebaseio.com/mix.json',form.value).subscribe((response)=>{
					
					this.isWait = false;
					this.hasSent = true;
					setTimeout(()=>{   
					  this.hasSent = false;
					},3000);
					this.ngOnInit();
					form.reset();
					
		 }, (error)=>{
				this.isWait = false;
				this.errorOccured = true;
				
				setTimeout(()=>{   
					  this.errorOccured = false;
			    },3000);
		 })
		
		
  }
  
  
    onLogout()
	{

		localStorage.removeItem("admin");
		
		this.router.navigate(['/admin']);
		
	
	}
  
  onDelete(mix)
  {
		
	  firebase.database().ref('mix/' + mix.key).remove();
	  this.ngOnInit();
		
  
  }
  
  ngOnInit() {
  
  
			if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
			
			if(localStorage.getItem("admin")=="yes")
			{
			
					this.countMixTapes =0;
					this.mixTapeLoaded = false;
					
					this.domRefresh = false;
					
					this.list = firebase.database().ref('/mix');
					this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
		  
					dataSnapshot.forEach((childSnapshot) => {
					  let person = childSnapshot.val();
					  person.key = childSnapshot.key
					  this.persons.push(person);
					  this.countMixTapes +=1;
					  	
						
					});
					
					
					
					if(this.persons.length == 0)
					{
					
						this.mixTapeLoaded = true;
						console.log("Length is 0");
					
					}else{
					
						if(this.countMixTapes == this.persons.length){
						
							this.mixTapeLoaded = true;
							this.domRefresh = true;
							
							this.zone.run(() => {});
							
						
						}
					
					}
					
					
					

				  });
					
			
			
			}
  
		
  }
  
  getFile(event)
  {
	
	//let data = 'data:application/octet-stream;base64,'+path;
	//console.log(event.target.files[0]);
	let data = 'data:application/octet-stream;base64,'+event.target.files[0].name;
	let storageRef = this.firebaseApp.storage().ref().child('events');
		
		console.log(event.target.files.item(0));
	
      storageRef.put(event.target.files[0]).then( snapshot => {
        console.log('successfully added');        
      }).catch(err => { console.error("Whoupsss!", err) })
	
  }
  
  onUploadFile(form)
  {
     let description = form.value.desc;
	 console.log(this.selectedFiles.item(0));
	 if (this.selectedFiles.item(0))
      this.uploadpic(description); 
  }
  
  chooseFiles(event) {
    this.selectedFiles = event.target.value;
     
  }

  uploadpic(description) {
    let file = this.selectedFiles.item(0);
    let uniqkey = description;
    const uploadTask = this.storage.upload('/mix_tape/' + uniqkey, file);

    this.imgsrc = uploadTask.downloadURL();
	alert("Something..");
	console.log(this.imgsrc);
 
  }
  
  
  
  

}
