import { Component, OnInit, NgZone, Inject } from '@angular/core';

import * as firebase from 'firebase/app';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import {Http} from '@angular/http';
 

 
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	  events:any[];
	  event: any;
	   list:any;
   
	   list2:any;
	   mixtapes:any[];
   
	   countEvents: number;
	   countMixTapes:number;
	   eventLoaded: boolean = false;
	   mixTapeLoaded: boolean = false;
   
	   domRefresh: boolean = false;

      eventHasData:boolean = false;
	  
	  hasSent: boolean = false;
	  isWait: boolean  = false;
	  errorOccured: boolean = false;

	
    constructor( private zone : NgZone, private http : Http, public dialog: MatDialog) {
		
   }
   
   
   
   
   onSendMessage(form)
   {

	     this.isWait = true;
	
		/* this.http.post('https://jkpro-jk.firebaseio.com/messages.json',form.value).subscribe((response)=>{
					
					this.isWait = false;
					this.hasSent = true;
					setTimeout(()=>{   
					  this.hasSent = false;
					},4000);
					
					form.reset();
					
		 }, (error)=>{
				this.isWait = false;
				this.errorOccured = true;
				
				setTimeout(()=>{   
					  this.errorOccured = false;
			    },4000);
		 })*/
		 
		 
		
   
   }
  
  onEvent(event)
  {
		this.event = event;
		this.eventHasData = true;
  }

 ngOnInit() {
  
	this.countEvents = 0;
		this.eventLoaded = false;
		
		this.domRefresh = false;
			
		   this.list = firebase.database().ref('/events');
             this.list.on('value', (dataSnapshot)=> {
            this.events = [];
  
            dataSnapshot.forEach((childSnapshot) => {
			  this.countEvents +=1;
              this.events.push(childSnapshot.val());
			  

            });
			
			if(this.events.length == 0)
			{
					
					this.eventLoaded = true;
			
			}else{
						
					 if(this.countEvents == this.events.length)
					  {
						  this.eventLoaded = true;
						  this.domRefresh = true;
						  
						  //this.events.sort();
						  //eventDate
						  this.events.sort(function(a, b) {
								var dateA:any = new Date(a.eventDate);
								var dateB:any = new Date(b.eventDate);
								return dateB - dateA;
							});
						   this.zone.run(() => {});
						   
						   
					  }
			
			}
			
			 
           
          });
		  
		  this.onLoadMixTapes();
		  
		   this.loadScript("http://code.jquery.com/jquery-1.7.1.min.js");
			this.loadScript("./assets/fmt/bootstrap/js/bootstrap.js");
			this.loadScript("./assets/fmt/scripts/plugins.js");
			this.loadScript("./assets/fmt/scripts/script.js");

  
  }
  
  	 loadScript(url: string) {
			const body = <HTMLDivElement> document.body;
			const script = document.createElement('script');
			script.innerHTML = '';
			script.src = url;
			script.async = false;
			script.defer = true;
			body.appendChild(script);
	  }
	 
	 

  /*ngOnInit() {
    this.loadScript("./assets/js/jquery/jquery-2.2.4.min.js");
	this.loadScript("./assets/js/bootstrap/popper.min.js");
	this.loadScript("./assets/js/bootstrap/bootstrap.min.js");
	this.loadScript("./assets/js/plugins/plugins.js");
	this.loadScript("./assets/js/active.js");
	
  }*/
  
 

  
  onLoadMixTapes()
  {
		this.countMixTapes  = 0;
		this.mixTapeLoaded = false;
		
		
		this.list2 = firebase.database().ref('/mix');
            this.list2.on('value', (dataSnapshot)=> {
            this.mixtapes = [];
  
            dataSnapshot.forEach((childSnapshot) => {
			  let person = childSnapshot.val();
			  
			  this.countMixTapes+=1;
			  
              this.mixtapes.push(person);
	
            });
			
			
			if(this.mixtapes.length == 0)
			{
				this.mixTapeLoaded = true;
			
			}else{
					
					 if(this.countMixTapes == this.mixtapes.length)
					 {
					  
						this.mixTapeLoaded = true;
						this.mixtapes.reverse();
						this.zone.run(() => {});
					  
					 }
			
			}
			
			
			

          });
  
  
  }
  
  
  


}









