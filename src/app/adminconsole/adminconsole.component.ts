import { Component, OnInit, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
@Component({
  selector: 'app-adminconsole',
  templateUrl: './adminconsole.component.html',
  styleUrls: ['./adminconsole.component.css']
})
export class AdminconsoleComponent implements OnInit {

	  list:any;
	persons:any;
	
	 isCorrect: boolean  = false;
	 incorrect: boolean = false;
	 
	 countBookings: number;
  constructor(private router: Router, private zone : NgZone) { 
		
			 this.onGetBookingLength();
			 //this.onGetEvents();
			 //this.onGetMixTapes();
		
  }
	
	onSignIn(form)
	{
	
		
		if(form.value.key == this.persons[0].description )
		{
				
			
				localStorage.setItem("admin","yes");
				
		}else{
				 alert("Invalid key");
				localStorage.setItem("admin","no");
				
		}
		
		if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
	
	}
	
	onLogout()
	{

		localStorage.removeItem("admin");
		this.isCorrect = false;
		//this.router.navigate(['/admin']);
		
	
	}
  
  ngOnInit() {
  
			if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
		
			
					this.list = firebase.database().ref('/mixtape');
				 this.list.on('value', (dataSnapshot)=> {
				this.persons = [];
	  
				dataSnapshot.forEach((childSnapshot) => {
				  let person = childSnapshot.val();
				  person.key = childSnapshot.key
				  this.persons.push(person);
				
				
				});
				
				
			   
			  });
			  

  
  }
  
  onGetBookingLength()
  {	
					
			 this.countBookings = 0;
			 
			  this.list = firebase.database().ref('/bookings');
					 this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
					
					
					dataSnapshot.forEach((childSnapshot) => {
					
				
					 this.countBookings +=1;

					});
					
					if(this.countBookings> 0)
					{
					   console.log("Counted");
					   this.zone.run(() => {});
					   console.log("Zone");
					}
					
	
           
          });		
  
  }
  
  onGetEvents()
  {
      
	   this.countBookings = 0;
			 
			  this.list = firebase.database().ref('/events');
					 this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
					
					
					dataSnapshot.forEach((childSnapshot) => {
					
				
					 this.countBookings +=1;

					});
					
					
	
           
          });		
  
  }
  
  onGetMixTapes()
  {
		
		 this.countBookings = 0;
			 
			  this.list = firebase.database().ref('/mix');
					 this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
					
					
					dataSnapshot.forEach((childSnapshot) => {
					
				
					 this.countBookings +=1;

					});
					
					
	
           
          });	
	
  }
  onGetMessages()
  {
  
		
  
  }

}
