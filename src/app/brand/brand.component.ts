import { Component, OnInit, NgZone, Inject } from '@angular/core';

import * as firebase from 'firebase/app';

import {Http} from '@angular/http';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  fans:any;
   events:any[];
	  event: any;
	   list:any;
   
	   list2:any;
	   mixtapes:any[];
   
	   countEvents: number;
	   countMixTapes:number;
	   eventLoaded: boolean = false;
	   mixTapeLoaded: boolean = false;
   
	   domRefresh: boolean = false;

      eventHasData:boolean = false;
	  
	  hasSent: boolean = false;
	  isWait: boolean  = false;
	  errorOccured: boolean = false;
	  
  constructor(private zone : NgZone, private http : Http) { }

  ngOnInit() {
   /* this.fans =[
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				},
				{
					imgSrc: "assets/brand/img/product-01.jpg"
				}
			
	]*/
	
	
	this.countEvents = 0;
		this.eventLoaded = false;
		
		this.domRefresh = false;
			
		   this.list = firebase.database().ref('/fans');
             this.list.on('value', (dataSnapshot)=> {
            this.events = [];
  
            dataSnapshot.forEach((childSnapshot) => {
			  this.countEvents +=1;
              this.events.push(childSnapshot.val());
			  

            });
			
			if(this.events.length == 0)
			{
					
					this.eventLoaded = true;
			
			}else{
						
					 if(this.countEvents == this.events.length)
					  {
						  this.eventLoaded = true;
						  this.domRefresh = true;
						  
						  //this.events.sort();
						  //eventDate
						  this.events.sort(function(a, b) {
								var dateA:any = new Date(a.eventDate);
								var dateB:any = new Date(b.eventDate);
								return dateB - dateA;
							});
						   this.zone.run(() => {});
						   
						   
					  }
			
			}
			
			 
           
          });
  }






}
