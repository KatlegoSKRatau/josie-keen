import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BookingComponent } from './booking/booking.component';
import { RouterModule } from '@angular/router';
import { AdminconsoleComponent } from './adminconsole/adminconsole.component';
import { BookingsComponent } from './bookings/bookings.component';
import { EventsComponent } from './events/events.component';
import * as firebase from "firebase";
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';

import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';
import { HttpModule } from '@angular/http';
import { MixtapeComponent } from './mixtape/mixtape.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { SpinnerModule } from 'angular2-spinner/dist';
import { MessagesComponent } from './messages/messages.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrandComponent } from './brand/brand.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CartComponent } from './cart/cart.component';
import { MatTableModule } from '@angular/material/table';
import { FansComponent } from './fans/fans.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

  export const config = {
  apiKey: "AIzaSyC5My7brjI_MxC_A7BdjcHlgHdobmYgLjI",
    authDomain: "jkpro-jk.firebaseapp.com",
    databaseURL: "https://jkpro-jk.firebaseio.com",
    projectId: "jkpro-jk",
    storageBucket: "jkpro-jk.appspot.com",
    messagingSenderId: "909716857282"
  };
  
   firebase.initializeApp(config);

  
  
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BookingComponent,
    AdminconsoleComponent,
    BookingsComponent,
    EventsComponent,
    MixtapeComponent,
    NotfoundComponent,
    MessagesComponent,
    BrandComponent,
    CartComponent,
    FansComponent
  ],
  imports: [
    SpinnerModule,
    BrowserModule,
	FormsModule,
	AngularFireModule.initializeApp(config),
	AngularFireDatabaseModule,
	AngularFireStorageModule,
	BrowserAnimationsModule,
	MatTableModule,
	MatTooltipModule,
	MatDialogModule,
	MatProgressSpinnerModule,
	HttpModule,
		RouterModule.forRoot([
			{path: '', component: HomeComponent},
			{path: 'booking', component: BookingComponent},
			{path: 'admin', component: AdminconsoleComponent},
			{path: 'bookingsadmin', component: BookingsComponent},
			{path: 'eventsadmin', component: EventsComponent},
			{path: 'mixtape', component: MixtapeComponent},
			{path: 'messagesadmin', component: MessagesComponent},
			{path: 'fans', component: BrandComponent},
			{path: 'cart', component: CartComponent},
			{path: 'fanadmin', component: FansComponent},
			{path: '**', component: NotfoundComponent}
	])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
