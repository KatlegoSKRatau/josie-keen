import { Component, OnInit, NgZone } from '@angular/core';
 import * as firebase from 'firebase/app';
 import { Router } from '@angular/router';
@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
	list:any;
	persons:any;
	
	isCorrect:boolean = false;
	countBookings: number;
	bookingsLoaded: boolean ;
	
	domRefresh: boolean = false;
	
    constructor(private router : Router, private zone: NgZone) { 

    }
  
  onDelete(booking)
  {
		
	    firebase.database().ref('bookings/' + booking.key).remove();
		this.ngOnInit();
  }
  
    onLogout()
	{

		localStorage.removeItem("admin");

		this.router.navigate(['/admin']);
		
	
	}

  ngOnInit() {
  
         
  
	      if(localStorage.getItem("admin")=="no")
			{
				this.isCorrect = false;
			
			}else if(localStorage.getItem("admin")=="yes"){
					
				this.isCorrect = true;
			}
			
			if(localStorage.getItem("admin")=="yes"){
				    
					 this.countBookings = 0;
					 this.bookingsLoaded = false;
					 this.domRefresh = false;
					
					
					this.list = firebase.database().ref('/bookings');
					 this.list.on('value', (dataSnapshot)=> {
					this.persons = [];
					
					
					dataSnapshot.forEach((childSnapshot) => {
					
					 let person  = childSnapshot.val();
					 person.key = childSnapshot.key;
					 this.countBookings +=1;
					 this.persons.push(person);
					 
								
							 
							 
							 

					});
					
					    if(this.persons.length == 0)
						{
						
							this.bookingsLoaded = true;
						
						}else{
						
							
							if(this.countBookings == this.persons.length )
							 {
										
										this.bookingsLoaded = true;
										this.domRefresh = true;
										
										this.zone.run(() => {});
										
							 }
						
						
						}
					
					     
			
           
          });
			
			
			
			}
		
		
		
         
  }

}
